package hawkEye;

import java.util.Scanner;

public class ReadInput {
	public String getInput() {
		Scanner scanner = new Scanner(System.in);
		String input = scanner.nextLine();
		return input;
		
	}
}
