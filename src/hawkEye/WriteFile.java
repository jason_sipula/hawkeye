package hawkEye;

import java.io.BufferedWriter;
import java.io.FileWriter;

public class WriteFile {
	public boolean writeFile(String writeMe, String saveDirectory, String fileNameTail) {
		boolean success;
		DateStamp dateStamp = new DateStamp();
		// Begin Write to XML Section
			String fileName = null;
			String randomName = null;
			String saveFile = null;
			randomName = Long.toHexString(Double.doubleToLongBits(Math.random()));
			fileName = dateStamp.getDateStamp("yyyyMMdd_hhmmss", "PST") + "_" + randomName + "_" + fileNameTail;
			saveFile = saveDirectory + fileName;
		try{
			FileWriter fStream = new FileWriter(saveFile);
			BufferedWriter out = new BufferedWriter(fStream);
			out.write(writeMe);
			out.close();
			success = true;
		}catch (Exception e) {
			System.err.println("Error: " + e.getMessage());
			success = false;
			return success;
		}
		// End Write to XML Section
		return success;
	}
}
