package hawkEye;

public class ResponseTimeAverager {
	public long[] createResponseTimeArray(int length) {
		long[] responseTimeArray = new long[length];
		return responseTimeArray;
	}
	public long[] createTotalResponseTimeArray(int total) {
		long[] totalResponseTimeArray = new long[total];
		return totalResponseTimeArray;
	}
	public long responseTimeAverager(final int testcount, long[] responseTimeArray) {
		long temp = 0;
		long averageResponseTime;
		for (int i = 0; i < responseTimeArray.length; i++) {
			temp += responseTimeArray[i];
		}
		averageResponseTime = (temp / (responseTimeArray.length + 1));
		return averageResponseTime;
	}
}
