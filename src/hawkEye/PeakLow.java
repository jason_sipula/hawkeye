package hawkEye;

public class PeakLow {
	public long getPeakTime(long[] totalResponseTimeArray) {
		long peakTime = totalResponseTimeArray[0];
		for (int i = 0; i < totalResponseTimeArray.length; i++) {
			if (totalResponseTimeArray[i] > peakTime) {
				peakTime = totalResponseTimeArray[i];
			}
		}
		return peakTime;
	}
	public long getLowTime(long[] totalResponseTimeArray) {
		long lowTime = totalResponseTimeArray[0];
		for (int i = 0; i < totalResponseTimeArray.length; i++) {
			if (totalResponseTimeArray[i] < lowTime) {
				lowTime = totalResponseTimeArray[i];
			}
		}
		return lowTime;
	}
}
