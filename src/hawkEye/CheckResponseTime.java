package hawkEye;

public class CheckResponseTime {
	public int checkResponeTime(final long responseTime) {
		int warnLevel = 0;
		if (responseTime >= 100) {
			warnLevel = 1;
		} else if (responseTime >= 110) {
			warnLevel = 2;
		}
		return warnLevel;
	}
}
