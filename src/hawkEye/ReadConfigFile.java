package hawkEye;

import java.io.File;
import java.io.FileFilter;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class ReadConfigFile {
	public File[] loadXml(String configDirectory, String configName) {
		File dir = new File(configDirectory);
		FileFilter fileFilter = new WildcardFileFilter(configName);
		File[] files = dir.listFiles(fileFilter);
		return files;
	}
	private String getTagValue(String sTag, Element eElement) {
		Node nValue = null;
		String returnString = null;
		try {
			NodeList nlList = eElement.getElementsByTagName(sTag).item(0).getChildNodes();
			if (nlList.getLength() < 1) {
				return returnString = "";
			}
			nValue = (Node) nlList.item(0);
			returnString = nValue.getNodeValue();
		} catch (Exception e) {
			e.printStackTrace();
			return returnString = "EXCEPTION at getTagValue()";
			
		}
		return returnString;
	}
	
}
