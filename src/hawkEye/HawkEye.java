package hawkEye;

import org.apache.log4j.Logger;

public class HawkEye {
	private static Logger log = Logger.getLogger(HawkEye.class);
	public static void main (String[] args) {
		log.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		log.info("Starting Program");
		log.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		programLaunch();
		UserInput input = userInput();
		driver(input.url, input.maxTestCount, input.averageCount);
		log.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		log.info("Finished Program");
		log.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
	}
	public static void programLaunch() {
		String versionNum = "0.1.4";
		System.out.println("");
		System.out.println("#############################################################");
		System.out.println("#                                                           #");
		System.out.println("# ... Welcome to HawkEye -- Website Monitoring Software ... #");
		System.out.println("# ......... Written by Jason Sipula || Sipula.net ......... #");
		System.out.println("# .................... Version : " + versionNum + " .................... #");
		System.out.println("#                                                           #");
		System.out.println("#############################################################");
		System.out.println("");
		System.out.println("This program will perform the following:");
		System.out.println("");
		System.out.println(" - Monitor HTTP Response codes");
		System.out.println(" - Calculate average response (latency) times");
		System.out.println(" - Log the results to results.log file");
	}
	public static UserInput userInput() {
		ReadInput readInput = new ReadInput();
		UserInput input = new UserInput();
		boolean urlSet = false;
		boolean maxSet = false;
		boolean avgSet = false;
		while (!(urlSet)) {
			System.out.println("");
			System.out.print("Please enter the website url you wish to monitor: ");
			input.url = readInput.getInput();
			if (input.url.equalsIgnoreCase("")) {
				urlSet = false;
				System.out.println("You have entered an invalid url, please try again...");
			} else {
				urlSet = true;
			}
		}
		log.info("URL to Test = " + input.url);
		while (!(maxSet)) {
			System.out.println("");
			System.out.print("Please enter the number of test cycles to perform (recommened 250): ");
			try {
				input.maxTestCount = Integer.parseInt(readInput.getInput());
				if (input.maxTestCount < 1) {
					maxSet = false;
					System.out.println("You have entered an invalid number of tests, please try again...");
				} else {
					maxSet = true;
				}
			} catch (NumberFormatException numException) {
				maxSet = false;
				System.out.println("You have entered an invalid number of tests, please try again...");
				continue;
			}
		}
		log.info("Number of Tests to Run = " + input.maxTestCount);
		while (!(avgSet)) {
			System.out.println("");
			System.out.print("Please enter the number of test cycles to averaged (recommended 50): ");
			try {
				input.averageCount = Integer.parseInt(readInput.getInput());
				if (input.averageCount < 1) {
					avgSet = false;
					System.out.println("You have entered an invalid number of tests, please try again...");
				} else {
					avgSet = true;
				}
			} catch (NumberFormatException numException) {
				System.out.println("You have entered an invalid number of tests, please try again...");
			}
		}
		log.info("Running Average Test Count = " + input.averageCount);
		log.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		System.out.println("");
		return input;
	}
	public static void driver(String url, int maxTestCount, int averageCount) {
		HTTPMon httpmon = new HTTPMon();
		ResponseTimeAverager responsetimeaverager = new ResponseTimeAverager();
		PeakLow peakLow = new PeakLow();
		long[] responseTimeArray = responsetimeaverager.createResponseTimeArray(averageCount);
		long[] totalResponseTimeArray = responsetimeaverager.createTotalResponseTimeArray(maxTestCount);
		byte testcount = 0;
		long responseTime = 0;
		long averageResponseTime = 0;
		String displayMe = "";
		System.out.println(" - Monitoring Website: " + url);
		System.out.println("");
		for (int i = 0; i < maxTestCount; i++) {
			responseTime = httpmon.pingUrl(url);
			if (responseTime == 0) {
				responseTime = averageResponseTime;
			}
			totalResponseTimeArray[i] = responseTime;
			responseTimeArray[testcount] = responseTime;
			averageResponseTime = responsetimeaverager.responseTimeAverager(testcount, responseTimeArray);
			testcount++;
			if (testcount == averageCount) {
				testcount = 0;
			}
			if (i < averageCount) {
				displayMe = "Avg Time (ms): " + "N/A" + " | Current Time (ms): " 
						+ responseTime + " | Current Test Cycle: " + (i +1) + "    ";
				if (displayMe.length() > 80) {
					displayMe = displayMe.substring(0, 80);
				}
			} else {
				displayMe = "Avg Time (ms): " + averageResponseTime + " | Current Time (ms): " 
						+ responseTime + " | Current Test Cycle: " + (i +1) + "    ";
				if (displayMe.length() > 80) {
					displayMe = displayMe.substring(0, 80);
				}
			}
			System.out.print(displayMe + "\r");
			log.info(displayMe);
		}
		//System.out.println("");
		System.out.println("\n");
		System.out.println(" - Test Cycles Completed, Calculating Statistics - \n");
		log.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		log.info(" - Test Cycles Completed, Calculating Statistics");
		System.out.println(" - " + maxTestCount + " Test Cycle Statistics:\n");
		log.info(" - " + maxTestCount + " Test Cycle Statistics:");
		displayMe = "Avg Time (ms): " + responsetimeaverager.responseTimeAverager(maxTestCount, totalResponseTimeArray) +
				" | Peak Time (ms): " + peakLow.getPeakTime(totalResponseTimeArray) + 
				" | Low Time (ms): " + peakLow.getLowTime(totalResponseTimeArray);
		System.out.println(displayMe);
		log.info(displayMe);
		System.out.println("");
	}
}
