package hawkEye;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class HTTPMon {
	public long pingUrl(final String address) {
		try {
			final URL url = new URL("http://" + address);
			final HttpURLConnection urlConn = (HttpURLConnection) url.openConnection();
			urlConn.setConnectTimeout(9999); // Timeout in MilliSeconds
			final long startTime = System.currentTimeMillis();
			urlConn.connect();
			final long endTime = System.currentTimeMillis();
			if (urlConn.getResponseCode() == HttpURLConnection.HTTP_OK) {
				long responseTime = (endTime - startTime);
//				if (responseTime < 5) {
//					responseTime = 999;
//				}
//				System.out.print("Time (ms) : " + responseTime);
//				System.out.print(" - Ping to " + address + " was success\r");
				return responseTime;
			}
			if (urlConn != null) {
				urlConn.disconnect();
			}
		} catch (final MalformedURLException e1) {
			System.out.println("You have entered an invalid url, please try again...");
			System.exit(0);
		} catch (final IOException e) {
			System.out.println("You have entered an invalid url, please try again...");
			System.exit(0);
		}
		return 9999L;
	}
}
